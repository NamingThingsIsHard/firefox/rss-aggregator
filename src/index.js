// rss-aggregator
// Copyright (C) 2019  NamingThingsIsHard / firefox
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
const DB_KEY = "aggregated-rss";
const AGGREGATE_INTERVAL = 60000;
const CHECK_INTERVAL = 5000;
/**
 * Time in days after which an item is kept or even allowed into the DB.
 * @type {number}
 */
const DEFAULT_EXPIRY_DAYS = 7;
const EXPIRY_KEY = "expiry_days";
const DAY_IN_MILLI = 86400000;

/**
 * Checked URLs with when they were checked in unix timestamp milliseconds
 * @type {Object.<String, Number>}
 */
let checkedUrls = {};
let checkingUrls = new Set();

/**
 * A single item in the rss feed.
 *
 * Only certain fields are used, but for more
 * see {@link https://cyber.harvard.edu/rss/rss.html#ltpubdategtSubelementOfLtitemgt|item} for more fields
 *
 * @typedef {Object} FeedItem
 *
 * @property {String} pubDate - Should be a parsable datetime
 * @property {String} isoDate - Not always present, so fallback to {@see pubDate}
 */

/**
 * @typedef {Object} RssJson
 *
 * @property {FeedItem[]} items
 */

/**
 * @typedef {String} UrlString
 */

const RSS_PARSER = new RSSParser();

/**
 * Promisify RSSParser.parseURL
 *
 * @param url
 * @returns {Promise<RssJson, String>} Errors should be simple strings
 */
function parseRSS(url) {
    return new Promise((accept, reject) => {
        checkingUrls.add(url);
        RSS_PARSER.parseURL(url, function (err, parsed) {
            checkedUrls[url] = Date.now();
            checkingUrls.has(url) && checkingUrls.delete(url);
            if (err) {
                return reject(err)
            } else {
                accept(parsed)
            }
        })
    })

}

/**
 * @typedef {Object.<String, RssJson>} Successes
 *
 * A URL to RSS JSON pairing
 */

/**
 * @typedef {Object} BestEffortError
 *
 * @property {String} url
 * @property {String} error
 */

/**
 * Best effort parse of all given URLS
 *
 * @param urls
 * @returns {Promise<(Successes, BestEffortError[])>}
 */
function parseAllRSS(urls) {

    return new Promise((accept) => {
        let leftToProcess = urls.length;
        let successes = {};
        let errors = [];
        if (urls.length < 1) {
            accept([successes, errors]);
            return
        }
        // TODO simplify this once Promise.allSettled arrives
        // TODO or with a custom implementation thereof
        for (let url of urls) {
            let isOld = url in checkedUrls;
            if (
                (isOld && (Date.now() - checkedUrls[url]) < AGGREGATE_INTERVAL)
                || checkingUrls.has(url)
            ) {
                --leftToProcess
                continue
            }
            parseRSS(url)
                .then((parsed) => {
                    successes[url] = parsed;
                    if (--leftToProcess <= 0) {
                        accept([successes, errors])
                    }
                })
                .catch((error) => {
                    errors.push({url, error});
                    if (--leftToProcess <= 0) {
                        accept([successes, errors])
                    }
                });
        }
        if (leftToProcess <= 0) {
            accept([successes, errors]);
        }
    })
}

/**
 * @typedef {Object} DbFeedItem
 * @property {Number} datetime - unix time of when the
 * @property {String} url - the referenced article
 * @property {String} host - the cached host of the url
 * @property {String} feedUrl - where this item was recovered from
 * @property {Boolean} read - whether the item was read by the user
 * @property {Boolean} starred - do we love this item and wanna keep it around?
 */

/**
 * @typedef {Object.<UrlString, DbFeedItem>} DbAggregation
 */

/**
 * Utility function
 * @param url {String}
 * @returns {String | null}
 */
function getHost(url) {
    let host = null;
    try {
        host = (new URL(url)).host
    } catch (e) {
    }
    return host
}

/**
 * @param {DbAggregation} old
 * @param {Successes} successes
 * @param {Number} expiryDateMs
 * @return {Number} new item count
 */
function mergeOldWithNewItems(old, successes, expiryDateMs) {
    let newItems = 0;
    for (let rssUrl in successes) {

        successes[rssUrl].items
            // Add only truly new items
            // No need to update the old ones (hence the filter)
            .filter(item => !old[item.link])
            // Remove urls with invalid hosts
            .filter(item => {
                if (!item.host) {
                    item.host = getHost(item.link)
                }
                return item.host
            })
            // Only items with valid, fresh dates
            .filter((item) => {
                item.datetime = Date.parse(item.isoDate || item.pubDate);
                return !isNaN(item.datetime) && !shouldClean(item, expiryDateMs)
            })
            .forEach((item) => {
                newItems++;
                old[item.link] = {
                    host: item.host,
                    title: item.title,
                    url: item.link,
                    description: item.description,
                    datetime: item.datetime,
                    feedUrl: rssUrl,
                    read: false,
                }

            })
    }
    return newItems
}

/**
 * Starred and unexpired items shouldn't be cleaned
 * @param item {DbFeedItem}
 * @param expiryDateMs {Number}
 * @returns {boolean}
 */
function shouldClean(item, expiryDateMs) {
    return !item.host || (!item.starred && item.datetime <= expiryDateMs)
}

/**
 *
 * @param db {DbFeedItem}
 * @param expiryDateMs {Number}
 * @return {Number} cleaned item count
 */
function cleanExistingItems(db, expiryDateMs) {
    let cleaned = 0;
    Object.keys(db).forEach((url) => {
        if (shouldClean(db[url], expiryDateMs)) {
            delete db[url];
            cleaned++;
        }
    })
    if (cleaned) {
        console.info(`Cleaned ${cleaned} items`);
    }
    return cleaned
}

let savingToDB = null;

/**
 * Updates the DB with parsed items
 * @param {Successes} successes
 */
async function saveToDB(successes) {
    if (savingToDB) {
        return
    }
    const [resExpiryDays, resAggregatedRSS] = await Promise.all([
        browser.storage.sync.get(EXPIRY_KEY),
        browser.storage.sync.get(DB_KEY)
    ]);
    const expiryDays = resExpiryDays[EXPIRY_KEY] || DEFAULT_EXPIRY_DAYS;
    const expiryDate = Date.now() - (expiryDays * DAY_IN_MILLI);

    let toSave = resAggregatedRSS[DB_KEY] || {};
    const deletedItemCount = cleanExistingItems(toSave, expiryDate);
    const newItemCount = mergeOldWithNewItems(toSave, successes, expiryDate);

    if (!deletedItemCount && !newItemCount) {
        return
    }
    savingToDB = await browser.storage.sync.set({[DB_KEY]: toSave});
    if (newItemCount > 0) {
        browser.notifications.create({
            title: "New feed items",
            type: "basic",
            message: `${newItemCount} new feed item(s)`,
            iconUrl: "/data/icon-64.png"
        })
    }
    savingToDB = null;
}

browser.browserAction.onClicked.addListener(() => {
    // Allowing only one tab
    let url = browser.extension.getURL("src/aggregator.html");
    browser.tabs.query({
        url: `${url}*`
    }).then((tabs) => {
        if (tabs.length > 0) {
            browser.tabs.update(tabs[0].id, {
                active: true
            });
        } else {
            browser.tabs.create({
                url: url
            })
        }
    })
})

async function aggregateFeeds() {
    const results = await browser.bookmarks.search("rss");
    let [successes, errors] = await parseAllRSS(
        results
            .filter(bookmark => bookmark.type === "bookmark")
            .map(bookmark => bookmark.url)
    );
    await saveToDB(successes);
    setTimeout(aggregateFeeds, CHECK_INTERVAL);
}

aggregateFeeds();

