// rss-aggregator
// Copyright (C) 2019  NamingThingsIsHard / firefox
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

const DB_KEY = "aggregated-rss";

angular.module("aggregatorApp", []).controller("AggregatorController", [
    "$scope",
    function ($scope) {
        this.dbItems = {};
        this.items = [];
        this.hosts = [];
        this.selectedHost = "All";
        this.clickToContainPort = browser.runtime.connect("@click-to-contain");
        this.showOption = "both";

        let booleanOptions = ["newestToOldest", "openInRandomContainer"];

        booleanOptions.forEach((booleanOption) => {
            this[booleanOption] = false;
            browser.storage.sync.get(booleanOption).then((res) => {
                let booleanValue = res[booleanOption];
                $scope.$apply(() => {
                    this[booleanOption] = booleanValue;
                })
                $scope.$watch(() => this[booleanOption], () => {
                    browser.storage.sync.set({
                        [booleanOption]: this[booleanOption]
                    })
                })
            })
        });

        // Update and register for changes to showOption
        browser.storage.sync.get("showOption").then(({showOption}) => {
            $scope.$apply(() => {
                this.showOption = showOption || this.showOption
            })
        })
        $scope.$watch(() => this.showOption, () => {
            browser.storage.sync.set({showOption: this.showOption})
        })

        // Force an update when we can't connect to the port
        this.clickToContainPort.onDisconnect.addListener(() => {
            $scope.$apply(() => {})
        })


        const update = (dbItems) => {
            dbItems = dbItems || {};
            const items = Object.values(dbItems);
            const hosts = new Set(items.map(item => item.host));

            $scope.$apply(() => {
                this.dbItems = dbItems;
                this.items = items;
                this.hosts = ["All", ...hosts];
            });
        }
        browser.storage.sync.get(DB_KEY).then((aggregated) => {
            update(aggregated[DB_KEY]);
        })

        browser.storage.onChanged.addListener((changes) => {
            const aggregatedChanges = changes[DB_KEY];
            if(!aggregatedChanges){
                return
            }
            update(aggregatedChanges.newValue);
        });

        this.toggleStarred = (item, starred) => {
            item.starred = starred !== undefined ? starred : !item.starred;
            browser.storage.sync.set({[DB_KEY]: this.dbItems})
        }

        this.toggleRead = (item, read) => {
            item.read = read !== undefined ? read : !item.read;
            browser.storage.sync.set({[DB_KEY]: this.dbItems})
        }

        this.openItemLink = (item) => {
            if (!this.clickToContainPort.error && this.openInRandomContainer) {
                this.clickToContainPort.postMessage({
                    command: "openTab",
                    data: {
                        url: item.url
                    }
                })
            } else {
                window.open(item.url)
            }
            this.toggleRead(item, true);
        }

        /**
         * Hide read items if the option is activated
         * @param {DbFeedItem} item
         * @returns {boolean}
         */
        this.hideItemFilter = (item) => {
            switch(this.showOption){
                case "both":
                    return true
                case "readOnly":
                    return item.read
                case "starredOnly":
                    return item.starred
                case "unreadOnly":
                    return !item.read
                default:
                    return true
            }
        }

        this.tabFilter = (item) => {
            return this.selectedHost === "All" ?
                true : this.selectedHost === item.host
        }
    }
])
